var offlineViewes = ["#favouritesOfflineView", "#detailedOfflineView"];

//Navigation stuff
function navigateToOfflineView(viewToNavigate){
    for(i = 0; i < offlineViewes.length; i++){
        if (offlineViewes[i] === viewToNavigate){
            $(viewToNavigate).show();
        }else{
            $(offlineViewes[i]).hide();
        }
    }
    if(viewToNavigate === "#favouritesOfflineView"){
        $("#viewNameOffline").html("Your Favourites TV Shows");
    }
}
navigateToOfflineView("#favouritesOfflineView");

function getSeasons(tvShowId){
    return JSON.parse(localStorage.getItem(tvShowId));
}

function writeSeasons(tvShowId, seasons){
    localStorage.setItem(tvShowId, JSON.stringify(seasons));
}

function updateOfflineFavourites(){
    $("#favouritesOfflineList").html("");
    if(localStorage.getItem("favourites") === null){
        var favouritesList = [];
        localStorage.setItem("favourites", JSON.stringify(favouritesList));
    }
    var favouritesList = JSON.parse(localStorage.getItem("favourites"));
    if (favouritesList.length === 0){
        $("#favouritesOfflineList").append(
            '<li class="list-group-item">Nothing in favourites</li>'
        );
    }else{
        for (i = 0; i < favouritesList.length; i ++){
            var tmpSeasons = getSeasons(favouritesList[i].id);
            var maxSeriesCount = 0;
            var viewedSeriesCount = 0;
            for(j = 0; j < tmpSeasons.length; j++){
                maxSeriesCount += parseInt(tmpSeasons[j][0]);
                viewedSeriesCount += parseInt(tmpSeasons[j][1]);
            }
            var backMessage = "";
            if ((maxSeriesCount - viewedSeriesCount) == 0){
                backMessage = "Fully watched";
            }else{
                backMessage = (maxSeriesCount - viewedSeriesCount) + " ep. left";
            }
            $("#favouritesOfflineList").append(
                '<button onclick="showOfflineDetailedInfo(' + favouritesList[i].id + ')" class="list-group-item">'
                + favouritesList[i].name + ' (' + backMessage + ') </button>'
            );
        }
    }
}
updateOfflineFavourites();

function showOfflineDetailedInfo(id){
    var favouritesList = JSON.parse(localStorage.getItem("favourites"));
    var filmToShow;
    for (i = 0; i < favouritesList.length; i ++){
        if(favouritesList[i].id === id){
            filmToShow = favouritesList[i];
            break;
        }
    }
    var genresStringLine = "";
    for(i = 0; i < filmToShow.genres.length; i++){
        genresStringLine += filmToShow.genres[i].name + " ";
    }
    
    $("#viewNameOffline").html(filmToShow.name);
    $("#descriptionOffline").html(filmToShow.overview);
    $("#offlinePopularity").html(filmToShow.popularity);
    
    if(filmToShow.original_language === "en"){
        $("#offlineLanguage").html("English");
    }else{
        $("#offlineLanguage").html(filmToShow.original_language);
    }
    $("#offlineGenres").html(genresStringLine);
    $("#offlineFE").html(filmToShow.first_air_date);
    $("#offlineLE").html(filmToShow.last_air_date);
    $("#offlineNOS").html(filmToShow.number_of_seasons);
    if(filmToShow.number_of_episodes !== null){
        $("#offlineNOE").html(filmToShow.number_of_episodes);
    }
    if(filmToShow.in_production === true){
        $("#offlineProduction").html("In production");
    }
    
    $("#offlineWatchlist").html("");
    var seasonsToShow = getSeasons(id);
    for(season = 0; season < seasonsToShow.length; season++){
        var seasonName = "";
        if(season === 0 && filmToShow.seasons[0].season_number === 0){
            seasonName = "Season Specials";
        }else{
            seasonName = "Season " + filmToShow.seasons[season].season_number;
        }
        $("#offlineWatchlist").append('<form>'+
                                           '<label for="offlineslider-' + season + '"> '+ seasonName + '</label>'+
                                           '<input type="number" data-type="range" name="offlineslider-' + season + '" id="offlineslider-' + season + '" '+
                                           'min="0" max="' + seasonsToShow[season][0] + '" value="' + seasonsToShow[season][1] + '">'+
                                           '</form>');
        var oflineSliderId = "#offlineslider-" + season;
        $(oflineSliderId).slider().slider('refresh');
    }

    $("#offlineWatchlist").change(function() {
        for(season = 0; season < seasonsToShow.length; season++){
            var sliderID = "#offlineslider-" + season;
            seasonsToShow[season][1] = $(sliderID).val();
        }
        writeSeasons(id, seasonsToShow);
        updateOfflineFavourites();
    });
    
    navigateToOfflineView("#detailedOfflineView");
}