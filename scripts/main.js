var views = ["#mainView", "#favouritesView", "#searchView", "#detailedInfoView"];
var topTVShowsData;
var currentlyShownTVShow;
var seasonsToShow;

function TVShowBasicImpl(id, name) {
    this.id = id;
    this.name = name;
};

function TVShowFullImpl(id, name, overview, popularity, original_language, number_of_seasons, number_of_episodes, genres, first_air_date, last_air_date, in_production, seasons){
    this.id = id;
    this.name = name;
    this.overview = overview;
    this.popularity = popularity;
    this.original_language = original_language;
    this.number_of_seasons = number_of_seasons;
    this.number_of_episodes = number_of_episodes;
    this.genres = genres;
    this.first_air_date = first_air_date;
    this.last_air_date = last_air_date;
    this.in_production = in_production;
    this.seasons = seasons;
}

function TVShowSeasons(tvShowID, seasons){
    this.tvShowID = tvShowID;
    var seasonsArray = new Array(seasons.length);
    for(seasonNumber = 0; seasonNumber < seasons.length; seasonNumber++){
        seasonsArray[seasonNumber] = [seasons[seasonNumber].episode_count, 0];
    }
    this.status_list = seasonsArray;
}

//Navigation stuff
function navigateTo(pageToNavigate){
    for(i = 0; i < views.length; i++){
        if (views[i] === pageToNavigate){
            $(pageToNavigate).show();
        }else{
            $(views[i]).hide();
        }
    }
};

navigateTo("#mainView"); // We want start from mainView

// Initializing swiper
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 50,
    onSlideChangeEnd: showMovieInfo
});

function showMovieInfo() {
    var currentFilm = topTVShowsData.results[swiper.activeIndex];
    currentlyShownTVShow = new TVShowBasicImpl(currentFilm.id, currentFilm.name);
    $("#tvShowName").html(currentFilm.name);
    $("#paragr").html(currentFilm.overview);
    $("#tvShowPopularity").html(Math.round(currentFilm.popularity));
    $("#tvShowAvgRating").html(currentFilm.vote_average);
    $("#tvShowOriginalLang").html(currentFilm.original_language);
    $("#tvShowPosition").html(swiper.activeIndex+1);
    $("#tvShowCountry").html(currentFilm.origin_country[0]);
    $("#tvShowFirstAirDate").html(currentFilm.first_air_date);
    $("#tvShowVotesCount").html(currentFilm.vote_count);
    $("#tvShowPoster").attr("src", "https://image.tmdb.org/t/p/w185" + topTVShowsData.results[swiper.activeIndex].poster_path);
    $("#viewInDetails").attr("onclick", 'showDetailedViewAbout(' + currentFilm.id + ')');
    
    $.ajax({
    url: "https://www.googleapis.com/youtube/v3/search",
    data: {
        part: "snippet",
        q: currentFilm.name + " trailer",
        key: "AIzaSyCtQcjqZoNN6C8vSz5QAakB0I50U-jiVT4"
    },
    success: function( data ) {
        var videoUrl = "https://www.youtube.com/embed/" + data.items[0].id.videoId;
        $("#tvShowTrailer").attr("src", videoUrl);
    }
    });
}

// Getting TOP TV shows content
$.ajax({
    url: "http://api.themoviedb.org/3/tv/popular",
    data: {
        api_key: "2f591c292e88d12cc343f2e4ab0838f2"
    },
    success: function( data ) {
        topTVShowsData = data;
        processDataToPage( topTVShowsData );
    }
});

function processDataToPage( data ) {
    for (i = 0; i < 10; i++) {
        $("#TVpop_" + i + "_poster").attr("src", "https://image.tmdb.org/t/p/original" + data.results[i].backdrop_path);
    }
    showMovieInfo();
}

//Getting up with orientation change
var previousOrientation = window.orientation;
var checkOrientation = function(){
    if(window.orientation !== previousOrientation){
        previousOrientation = window.orientation;
    }
};

window.addEventListener("resize", checkOrientation, false);
window.addEventListener("orientationchange", checkOrientation, false);

// (optional) Android doesn't always fire orientationChange on 180 degree turns
setInterval(checkOrientation, 2000);


//Search related stuff
var searchResultsData = [];
$("#searchResultsBlock").hide();

function showSearchResults(){
    var tmpTvShowElement;
    if (searchResultsData.length === 0){
        $("#searchResultsList").append(
            '<li><button class="ui-btn">No such TV Shows found</button></li>'
        );
    }
    for(i = 0; i < searchResultsData.length; i++){
        tmpTvShowElement = searchResultsData.pop();
        $("#searchResultsList").append(
            '<li><button onclick="showDetailedViewAbout(' + tmpTvShowElement.id + ')" class="ui-btn ui-btn-icon-right ui-icon-carat-r">' + tmpTvShowElement.name + '</button></li>'
        );
    }
};

function updateLastsearchList(){
    $("#lastSearchesList").html("");
    if(localStorage.getItem("searchHistory") === null){
        var pastRequests = [];
        localStorage.setItem("searchHistory", JSON.stringify(pastRequests));
        $("#lastSearchesList").append(
            '<li><button class="ui-btn">Nothing in search history</button></li>'
        );
    }else{
        var pastRequests = JSON.parse(localStorage.getItem("searchHistory"));
        for (i = 0; i < pastRequests.length; i ++){
            $("#lastSearchesList").append(
                '<li><button onclick="startSearch(\'' + pastRequests[i] + '\')" class="ui-btn ui-btn-icon-right ui-icon-carat-r">' + pastRequests[i] + '</button></li>'
            );
        }
    }
}

updateLastsearchList();

function saveSearchRequest(requestQuery){
    if(localStorage.getItem("searchHistory") === null){
        var pastRequests = [];
        localStorage.setItem("searchHistory", JSON.stringify(pastRequests));
    }
    var pastRequests = JSON.parse(localStorage.getItem("searchHistory"));
    pastRequests.reverse();
    pastRequests.push(requestQuery);
    pastRequests.reverse();
    //alert(pastRequests.length);
    if(pastRequests.length > 10){
        localStorage.setItem("searchHistory", JSON.stringify(pastRequests.slice(0, 9)));
    }else{
        localStorage.setItem("searchHistory", JSON.stringify(pastRequests));
    }
    updateLastsearchList();
};

function startSearch(stringQuery){
    $("#searchResultsBlock").show();
    $("#searchResultsList").html("");
    searchResultsData = [];
    if(stringQuery === ""){
        $("#searchResultsList").append(
            '<li><button class="ui-btn">Empty search query!</button></li>'
        );
    }else{
        saveSearchRequest(stringQuery);
        $.ajax({
            url: "http://api.themoviedb.org/3/search/tv",
            data: {
                query: stringQuery,
                api_key: "2f591c292e88d12cc343f2e4ab0838f2"
            },
            success: function( data ) {
                var searchResults = data.results
                for(i = 0; i < searchResults.length; i++){
                    searchResultsData.push(new TVShowBasicImpl(searchResults[i].id, searchResults[i].name));
                }
                showSearchResults();
            }
        });
    }
    
};

function startSearchFromInput(){
    startSearch($("#searchInput").val());
};


//Favourites view stuff
function updateFavourites(){
    $("#favouritesList").html("");
    if(localStorage.getItem("favourites") === null){
        var favouritesList = [];
        localStorage.setItem("favourites", JSON.stringify(favouritesList));
    }
    var favouritesList = JSON.parse(localStorage.getItem("favourites"));
    if (favouritesList.length === 0){
        $("#favouritesList").append(
            '<li><button class="ui-btn">Nothing in favourites</button></li>'
        );
    }else{
        for (i = 0; i < favouritesList.length; i ++){
            var tmpSeasons = getSeasons(favouritesList[i].id);
            var maxSeriesCount = 0;
            var viewedSeriesCount = 0;
            for(j = 0; j < tmpSeasons.length; j++){
                maxSeriesCount += parseInt(tmpSeasons[j][0]);
                viewedSeriesCount += parseInt(tmpSeasons[j][1]);
            }
            var backMessage = "";
            if ((maxSeriesCount - viewedSeriesCount) == 0){
                backMessage = "Fully watched";
            }else{
                backMessage = (maxSeriesCount - viewedSeriesCount) + " ep. left";
            }
            $("#favouritesList").append(
                '<li><button onclick="showDetailedViewAbout(' + favouritesList[i].id + ')" class="ui-btn ui-btn-icon-right ui-icon-carat-r">' 
                + favouritesList[i].name + ' (' + backMessage + ') </button></li>'
            );
        }
    }
}

updateFavourites();

function addSeasons(tvShowSeason){
    localStorage.setItem(tvShowSeason.tvShowID, JSON.stringify(tvShowSeason.status_list));
}

function removeSeasons(tvShowId){
    localStorage.removeItem(tvShowId);
}

function getSeasons(tvShowId){
    return JSON.parse(localStorage.getItem(tvShowId));
}

function writeSeasons(tvShowId, seasons){
    localStorage.setItem(tvShowId, JSON.stringify(seasons));
}

function addToFavourites(){
    addSeasons(new TVShowSeasons(currentlyShownTVShow.id, currentlyShownTVShow.seasons));    
    var favouritesList = JSON.parse(localStorage.getItem("favourites"));
    favouritesList.reverse();
    favouritesList.push(currentlyShownTVShow);
    favouritesList.reverse();
    if(favouritesList.length > 10){
        localStorage.setItem("favourites", JSON.stringify(favouritesList.slice(0, 9)));
    }else{
        localStorage.setItem("favourites", JSON.stringify(favouritesList));
    }
    $("#favouritesOptions").html(
        '<button onclick="removeFromFavourites()" class="ui-btn ui-btn-icon-right ui-icon-carat-r">Remove from favourites</button>'
    );
    $("#watchedSeries").show();
    updateFavourites();
    showDetailedViewAbout(currentlyShownTVShow.id)
}

function removeFromFavourites(){
    removeSeasons(currentlyShownTVShow.id);
    $("#watchedSeries").hide();
    var favouritesList = JSON.parse(localStorage.getItem("favourites"));
    favouritesList = favouritesList.filter(function(TVShow){return TVShow.id !== currentlyShownTVShow.id});
    localStorage.setItem("favourites", JSON.stringify(favouritesList));
    $("#favouritesOptions").html(
        '<button onclick="addToFavourites()" class="ui-btn ui-btn-icon-right ui-icon-carat-r">Add to favourites</button>'
    );
    updateFavourites();
}

function favouritesContainsThisShow(){
    var favouritesList = JSON.parse(localStorage.getItem("favourites"));
    for (i = 0; i < favouritesList.length; i++){
        if(favouritesList[i].id == currentlyShownTVShow.id){
            return true;
        }
    }
    return false;
}

//Detailed view stuff
function showDetailedViewAbout(tvShowId){
    var tvShowInfo;
    $.ajax({
        url: "http://api.themoviedb.org/3/tv/" + String(tvShowId),
        data: {
            api_key: "2f591c292e88d12cc343f2e4ab0838f2"
        },
        success: function( data ) {
            tvShowInfo = data;
            currentlyShownTVShow = new TVShowFullImpl(tvShowInfo.id, tvShowInfo.name, tvShowInfo.overview, tvShowInfo.popularity, tvShowInfo.original_language
                                                     ,tvShowInfo.number_of_seasons, tvShowInfo.number_of_episodes, tvShowInfo.genres, tvShowInfo.first_air_date
                                                     ,tvShowInfo.last_air_date, tvShowInfo.in_production, tvShowInfo.seasons);
            $("#tvShowNameDetailed").html(tvShowInfo.name);
            $("#tvShowPopularityDetailed").html(Math.round(tvShowInfo.popularity));
            $("#tvShowAvgRatingDetailed").html(tvShowInfo.vote_average);
            $("#tvShowOriginalLangDetailed").html(tvShowInfo.original_language);
            $("#tvShowCountryDetailed").html(tvShowInfo.origin_country[0]);
            $("#detailedViewDesc").html(tvShowInfo.overview);
            if(tvShowInfo.number_of_seasons !== null && tvShowInfo.number_of_episodes !== null){
                $("#tvShowSeasonsAndEpisodes").html("This TV show consists of " + tvShowInfo.number_of_seasons +
                                                    " season(s) with " + tvShowInfo.number_of_episodes + " episode(s) in total.");
            }
            if(tvShowInfo.genres.length !== 0){
                $("#tvShowGenresDetailed").html("It belongs to following genres: ");
                $("#tvShowGenresDetailed").append(" " + tvShowInfo.genres[0].name);
                for (i = 1; i < tvShowInfo.genres.length - 1; i++){
                    $("#tvShowGenresDetailed").append(", " + tvShowInfo.genres[i].name);
                }
                $("#tvShowGenresDetailed").append(".");
            }
            if(tvShowInfo.in_production === true){
                $("#isOngoing").html(" Current status: in production.");
            }else if (tvShowInfo.in_production === false){
                $("#isOngoing").html(" Current status: not in production.");
            }
            if(tvShowInfo.first_air_date !== null){
                $("#tvShowFirstAirDateDetailed").html("The first episode was out on " + tvShowInfo.first_air_date + ".");
            }
            if(tvShowInfo.last_air_date !== null){
                $("#tvShowLastAirDateDetailed").html(" Last episode date " + tvShowInfo.last_air_date + ".");
            }
            $("#tvShowVotesCountDetailed").html(tvShowInfo.vote_count);
            if (tvShowInfo.poster_path !== null){
                $("#tvShowPosterDetailed").attr("src", "https://image.tmdb.org/t/p/w185" + tvShowInfo.poster_path);
            }else{
                $("#tvShowPosterDetailed").attr("src", "/images/no_img.png");
            }
            
            if(favouritesContainsThisShow()){
                $("#watchedSeries").show();
                $("#detailedViewWatchlist").html("");
                seasonsToShow = getSeasons(tvShowId);
                for(season = 0; season < seasonsToShow.length; season++){
                    var seasonName = "";
                    if(season === 0 && tvShowInfo.seasons[0].season_number === 0){
                        seasonName = "Season Specials";
                    }else{
                        seasonName = "Season " + tvShowInfo.seasons[season].season_number;
                    }
                    $("#detailedViewWatchlist").append('<form>'+
                                                       '<label for="slider-' + season + '"> '+ seasonName + '</label>'+
                                                       '<input type="number" data-type="range" name="slider-' + season + '" id="slider-' + season + '" '+
                                                       'min="0" max="' + seasonsToShow[season][0] + '" value="' + seasonsToShow[season][1] + '">'+
                                                       '</form>');
                    var id = "#slider-" + season;
                    $(id).slider().slider('refresh');
                }

                $("#detailedViewWatchlist").change(function() {
                    for(season = 0; season < seasonsToShow.length; season++){
                        var sliderID = "#slider-" + season;
                        seasonsToShow[season][1] = $(sliderID).val();
                    }
                    writeSeasons(currentlyShownTVShow.id, seasonsToShow);
                    updateFavourites();
                });
            }else{
                $("#watchedSeries").hide();
            }
            $.ajax({
            url: "https://www.googleapis.com/youtube/v3/search",
            data: {
                part: "snippet",
                q: tvShowInfo.name + " trailer",
                key: "AIzaSyCtQcjqZoNN6C8vSz5QAakB0I50U-jiVT4"
            },
            success: function( data ) {
                var videoUrl = "https://www.youtube.com/embed/" + data.items[0].id.videoId;
                $("#detailedViewTrailed").attr("src", videoUrl);
                if(favouritesContainsThisShow()){
                    $("#favouritesOptions").html('<button onclick="removeFromFavourites()" class="ui-btn ui-btn-icon-right ui-icon-carat-r">Remove from favourites</button>');
                }else{
                    $("#favouritesOptions").html('<button onclick="addToFavourites()" class="ui-btn ui-btn-icon-right ui-icon-carat-r">Add to favourites</button>');
                }
                navigateTo("#detailedInfoView");
            }
            });
            
        }
        });
}